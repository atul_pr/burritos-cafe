package com.cafe.burrito.repository;

import com.cafe.burrito.model.Dish;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DishRepository extends MongoRepository<Dish, String> {
}
