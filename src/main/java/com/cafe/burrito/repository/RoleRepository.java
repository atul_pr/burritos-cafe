package com.cafe.burrito.repository;

import com.cafe.burrito.model.Customer;
import com.cafe.burrito.model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByRole(String role);
}
