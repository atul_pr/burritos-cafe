package com.cafe.burrito.repository;

import com.cafe.burrito.model.Cart;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface CartRepository extends MongoRepository<Cart, String> {

    List<Cart> findByCustomerId(String customerId);
    void deleteAllByCustomerId(String customerId);
}
