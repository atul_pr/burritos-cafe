package com.cafe.burrito.controller;

import com.cafe.burrito.model.Customer;
import com.cafe.burrito.model.Role;
import com.cafe.burrito.repository.CustomerRepository;
import com.cafe.burrito.repository.RoleRepository;
import com.cafe.burrito.service.CustomCustomerDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AuthController {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private RoleRepository roleRepository;

    @GetMapping("/login")
    public String login(){

        return "login";
    }

    @GetMapping("/register")
    public String getRegister(Model model){

        model.addAttribute("customer", new Customer());
        return "register";
    }

    @PostMapping("/register")
    public String postRegister(@ModelAttribute("customer") Customer customer, HttpServletRequest request, Model model) throws ServletException {

        if(customerRepository.findCustomerByEmail(customer.getEmail()).isPresent()){
            return "redirect:/login";
        }

        String password = customer.getPassword();
        customer.setPassword(bCryptPasswordEncoder.encode(password));
        List<Role> roles = new ArrayList<>();
        roles.add(roleRepository.findByRole("ROLE_CUSTOMER").get());
        customer.setRoles(roles);
        customerRepository.save(customer);
        request.login(customer.getEmail(),password);
        return "redirect:/";
    }
}
