package com.cafe.burrito.controller;

import com.cafe.burrito.model.Dish;
import com.cafe.burrito.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;

@Controller
public class DishController {

    @Autowired
    private DishService dishService;

    @GetMapping("/")
    public String welcome(Model model) {

        model.addAttribute("dishes", dishService.getAllDishes());
        return "index";
    }

    @GetMapping("/admin/adddish")
    public String getAddDish(Model model){

        model.addAttribute("dish", new Dish());
        return "adddish";
    }

    @PostMapping("/admin/adddish")
    public String postAddDish(@ModelAttribute("dish")Dish dish){

        dishService.addDish(dish);
        return "redirect:/";
    }

    @GetMapping("/dish/{id}")
    public String getDishById(@PathVariable("id") String id, Model model){

        model.addAttribute("dish", dishService.findDishById(id).get());
        return "dishdetail";
    }

}
