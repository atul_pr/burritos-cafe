package com.cafe.burrito.controller;

import com.cafe.burrito.model.Cart;
import com.cafe.burrito.model.CustomCustomerDetail;
import com.cafe.burrito.model.Customer;
import com.cafe.burrito.model.Dish;
import com.cafe.burrito.repository.DishRepository;
import com.cafe.burrito.service.CartService;
import com.cafe.burrito.service.CustomerService;
import com.cafe.burrito.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

@Controller
public class CartController {

    @Autowired
    private DishRepository dishRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private DishService dishService;

    @Autowired
    private CartService cartService;

    @GetMapping("/cart")
    public String getCart(Authentication authentication, Model model){
        CustomCustomerDetail customCustomerDetail = (CustomCustomerDetail) authentication.getPrincipal();
        String customerId = customCustomerDetail.getId();
        List<Cart> cartItems = cartService.getCart(customerId);
        double total = 0;
        for(Cart cart: cartItems){
            total += cart.getDish().getPrice();
        }
        model.addAttribute("total", total);
        model.addAttribute("totalItems", cartItems.size());
        model.addAttribute("cartItems", cartItems);

        return "cart";
    }

    @PostMapping("/addtocart/{dishId}")
    public String addToCart(@PathVariable String dishId, Authentication authentication){

        CustomCustomerDetail customCustomerDetail = (CustomCustomerDetail) authentication.getPrincipal();
        String customerId = customCustomerDetail.getId();
        Optional<Customer> customer = customerService.getCustomerById(dishId);

        Optional<Dish> dish = dishService.findDishById(dishId);
        if(dish.isEmpty()){
            return "redirect:/";
        }

        Cart cart = new Cart(dish.get(), customerId);
        cartService.addToCart(cart);

        return "redirect:/cart";
    }

    @GetMapping("/cart/{cartId}")
    public String deleteCartById(@PathVariable String cartId){
        cartService.deleteByCartId(cartId);
        return "redirect:/cart";
    }

    @GetMapping("/order")
    public String orderPlaced(Authentication authentication){

        CustomCustomerDetail customCustomerDetail = (CustomCustomerDetail) authentication.getPrincipal();
        String customerId = customCustomerDetail.getId();
        List<Cart> cartItems = cartService.getCart(customerId);
        if(cartItems.isEmpty()){
            return "redirect:/";
        }
        cartService.deleteAllByCustomerId(customerId);
        return "order";
    }
}
