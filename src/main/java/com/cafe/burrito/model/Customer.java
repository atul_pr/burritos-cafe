package com.cafe.burrito.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class Customer {

    @Id
    private String _id;
    private String email;
    private String password;
    private String name;

    @DBRef
    private List<Role> roles;

    public Customer() {

    }

    public Customer(String _id, String email, String password, String name, List<Role> roles) {

        this._id = _id;
        this.email = email;
        this.password = password;
        this.name = name;
        this.roles = roles;
    }

    public Customer(String email, String password, String name, List<Role> roles) {

        this.email = email;
        this.password = password;
        this.name = name;
        this.roles = roles;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "_id='" + _id + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", roles=" + roles +
                '}';
    }
}
