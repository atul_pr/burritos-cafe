package com.cafe.burrito.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CustomCustomerDetail extends Customer implements UserDetails {

    public CustomCustomerDetail(Customer customer){
        super(customer.get_id(), customer.getEmail(), customer.getPassword(), customer.getName(), customer.getRoles());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        super.getRoles().forEach((role) -> {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getRole()));
        });

        return grantedAuthorities;
    }

    @Override
    public String getUsername() {

        return super.getEmail();
    }

    @Override
    public String getPassword() {

        return super.getPassword();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    public String getId() {
        return super.get_id();
    }

    public String getName(){
        return super.getName();
    }
}
