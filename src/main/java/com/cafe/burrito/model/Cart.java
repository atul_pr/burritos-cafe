package com.cafe.burrito.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Cart {

    @Id
    private String _id;

    @DBRef
    private Dish dish;

    private String customerId;

    public Cart() {
    }

    public Cart(String _id, Dish dish, String customerId) {
        this._id = _id;
        this.dish = dish;
        this.customerId = customerId;
    }

    public Cart(Dish dish, String customerId) {
        this.dish = dish;
        this.customerId = customerId;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "_id='" + _id + '\'' +
                ", dish=" + dish +
                ", customerId='" + customerId + '\'' +
                '}';
    }
}
