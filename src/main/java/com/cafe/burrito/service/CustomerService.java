package com.cafe.burrito.service;

import com.cafe.burrito.model.Customer;
import com.cafe.burrito.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Optional<Customer> getCustomerById(String id){

        return customerRepository.findById(id);
    }
}
