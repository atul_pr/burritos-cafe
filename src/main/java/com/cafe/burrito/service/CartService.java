package com.cafe.burrito.service;

import com.cafe.burrito.model.Cart;
import com.cafe.burrito.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CartService {

    @Autowired
    private CartRepository cartRepository;

    public void addToCart(Cart cart){

        cartRepository.save(cart);
    }

    public List<Cart> getCart(String customerId){

        return cartRepository.findByCustomerId(customerId);
    }

    public Optional<Cart> getCartById(String cartId){

        return cartRepository.findById(cartId);
    }

    public void deleteByCartId(String cartId){

        cartRepository.deleteById(cartId);
    }

    public void deleteAllByCustomerId(String customerId){

        cartRepository.deleteAllByCustomerId(customerId);
    }
}
