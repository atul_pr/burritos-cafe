package com.cafe.burrito.service;

import com.cafe.burrito.model.Dish;
import com.cafe.burrito.repository.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DishService {

    @Autowired
    private DishRepository dishRepository;

    public List<Dish> getAllDishes(){

        return dishRepository.findAll();
    }

    public void addDish(Dish dish){

        dishRepository.save(dish);
    }

    public Optional<Dish> findDishById(String id){

        return dishRepository.findById(id);
    }

}
