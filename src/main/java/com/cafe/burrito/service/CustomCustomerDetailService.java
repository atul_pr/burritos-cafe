package com.cafe.burrito.service;

import com.cafe.burrito.model.CustomCustomerDetail;
import com.cafe.burrito.model.Customer;
import com.cafe.burrito.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomCustomerDetailService implements UserDetailsService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Optional<Customer> customer = customerRepository.findCustomerByEmail(email);
        customer.orElseThrow(()-> new UsernameNotFoundException("Email not found!"));

        return customer.map(CustomCustomerDetail::new).get();
    }
}


