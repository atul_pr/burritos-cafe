package com.cafe.burrito.service;

import com.cafe.burrito.model.Dish;
import com.cafe.burrito.repository.DishRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class DishServiceTest {

    @Mock
    private DishRepository dishRepository;

    @InjectMocks
    private DishService dishService;

    @Test
    void getAllDishes(){

        dishService.getAllDishes();
        verify(dishRepository).findAll();
    }

    @Test
    void addDish(){
        Dish dish = new Dish("Strawberry Pastry", 60, "https://ahaarbazaar.com/wp-content/uploads/2020/09/strawberry-mio-ab.jpg", "A delicious treat of sweet strawberries and cake");
        dishService.addDish(dish);
        ArgumentCaptor<Dish> dishArgumentCaptor = ArgumentCaptor.forClass(Dish.class);
        verify(dishRepository).save(dishArgumentCaptor.capture());
        Dish captureDish = dishArgumentCaptor.getValue();
        assertThat(captureDish).isEqualTo(dish);
    }

    @Test
    void findDishById(){
        dishService.findDishById(anyString());
        verify(dishRepository).findById(anyString());
    }
}
